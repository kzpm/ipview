## ipview

*ipview* is a script, similar to `ipconfig` in Windows, that tries to extract ip information from network related files within a device. 

The main goal of the project is to retrieve:

1. Ip address of device; 
2. Subnetmask 
3. Ipv6 address (GUA) 
4. Link local ipv6 address 
5. DNS server 
6. Gateway
7. MAC Address
